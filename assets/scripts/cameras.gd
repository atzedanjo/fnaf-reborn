extends Node2D

onready var static_anim = get_node("static/anim")
onready var animatronics = get_node("/root/office/animatronics")

#var waypoints = {
#	1 : [4],
#	2 : [0, 10, 20],
#	3 : [1, 2, 11, 21],
#}

onready var cams = {
	1 : get_node("1"),
	2 : get_node("2"),
	3 : get_node("3"),
	4 : get_node("4"),
	5 : get_node("5"),
	6 : get_node("6"),
	7 : get_node("7"),
	8 : get_node("8"),
	9 : get_node("9"),
	10 : get_node("10"),
	11 : get_node("11"),
	}

var current_cam = 1
var prev_cam = 1

func _ready():
	for node in animatronics.get_children():
		if node.get_name() == "sfx":
			continue
		node.connect("position_changed", self, "_on_animatronic_moved")
	connect("visibility_changed", self, "_on_visibility_changed")
	switch_to(2)

func _on_visibility_changed():
	if is_visible():
		get_node("Camera2D").make_current()
		get_node("hum").play()
	else:
		get_node("/root/office/view").make_current()
		get_node("hum").stop()

func _on_animatronic_moved(pos):
	for cam in get_children():
		if cam.has_node(str(pos)):
			static_anim.switch_anim("cam_switch")

func switch_to(n):
	if n == current_cam:
		return
	static_anim.switch_anim("cam_switch")
	prev_cam = current_cam
	current_cam = n
	cams[prev_cam].hide()
	cams[current_cam].show()
	get_node("Camera2D/map/" + str(prev_cam) + "/status").set_frame(0)
	get_node("Camera2D/map/" + str(current_cam) + "/status").set_frame(1)
	if current_cam in [1, 4, 7, 9, 10]:
		get_node("Camera2D").stop_movement()
	else:
		get_node("Camera2D").start_movement()