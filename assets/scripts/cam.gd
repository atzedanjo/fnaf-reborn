extends AnimatedSprite

onready var animatronics = get_node("/root/office/animatronics")
onready var cam_id = int(get_name())

func _ready():
	connect("visibility_changed", self, "_on_visibility_changed")

func _on_visibility_changed():
	if not is_hidden():
		set_fixed_process(true)
	else:
		set_fixed_process(false)

func _fixed_process(delta):
	for waypoint in get_children():
		if waypoint.get_name() == "banner":
			continue
		if waypoint.has_method("show"):
			if animatronics.is_animatronic_at(int(waypoint.get_name())):
				waypoint.show()
			else:
				waypoint.hide()

func black_out():
	get_node("anim").play("scare")