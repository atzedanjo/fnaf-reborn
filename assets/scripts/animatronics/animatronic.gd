extends Node

signal position_changed

var position setget set_position, get_position
var current_state
var prev_state
var path_tree = {}
var threat_pos
var block_node
var color

var STATES = {}

func _ready():
	path_tree = get_parent().data[int(get_name())].PATH
	position = get_parent().data[int(get_name())].START_POS
	threat_pos = get_parent().data[int(get_name())].THREAT_POS
	block_node = get_parent().data[int(get_name())].BLOCK_NODE
	color = get_parent().data[int(get_name())].COLOR
	STATES = {
		"IDLE" : get_node("state_idle"),
		"UNSEEN" : get_node("state_unseen"),
		"SEEN" : get_node("state_seen"),
		"WALK" : get_node("state_walk"),
		"THREAT" : get_node("state_threat"),
	}
	prev_state = STATES.IDLE
	if get_name() in ["0", "3"]:
		transition_to("IDLE")
	else:
		transition_to("UNSEEN")
	set_fixed_process(true)

func _fixed_process(delta):
	current_state.update(delta)

func transition_to(state):
	if STATES.has(state):
		prev_state = current_state
		current_state = STATES[state]
		if prev_state:
			prev_state.exit()
		current_state.enter()

func get_position():
	return position

func set_position(pos):
	position = pos
	emit_signal("position_changed", self.position)