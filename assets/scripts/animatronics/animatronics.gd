extends Node2D

var data = {
	# FREDDY
	0 : {
		"ID" : 0,
		"COLOR" : "#a84600",
		"START_POS" : 0,
		"THREAT_POS" : [98],
		"BLOCK_NODE" : "/root/office/hud/mask",
		"PATH" : {
			0 : [1],
			1 : [2],
			2 : [98],
			98 : [1],
		},
	},
	
	# BONNIE
	1 : {
		"ID" : 1,
		"COLOR" : "#0018f7",
		"START_POS" : 10,
		"THREAT_POS" : [99],
		"BLOCK_NODE" : "/root/office/hud/mask",
		"PATH" : {
			10 : [11],
			11 : [12],
			12 : [11, 13],
			13 : [12, 14],
			14 : [13, 99],
			99 : [14, 13, 12, 11]
		},
	},
	
	# CHICA
	2 : {
		"ID" : 2,
		"COLOR" : "#fffa00",
		"START_POS" : 20,
		"THREAT_POS" : [97],
		"BLOCK_NODE" : "/root/office/hud/mask",
		"PATH" : {
			20 : [21],
			21 : [22, 24],
			22 : [21, 23],
			23 : [22],
			24 : [97],
			97 : [21, 22, 23],
		},
	},
	
	# FOXY
	3 : {
		"ID" : 3,
		"COLOR" : "#ff1900",
		"START_POS" : 30,
		"THREAT_POS" : [96],
		"BLOCK_NODE" : "/root/office/hud/mask",
		"PATH" : {
			30 : [31],
			31 : [32],
			32 : [33],
			33 : [96],
			96 : [30],
		},
	},
	
	# SPRINGTRAP
	4 : {
		"ID" : 4,
		"COLOR" : "#06c600",
		"START_POS" : 40,
		"THREAT_POS" : [95, 94, 93],
		"BLOCK_NODE" : "/root/office/hud/mask",
		"PATH" : {
			40 : [41],
			41 : [42, 43],
			42 : [44, 47],
			43 : [46],
			44 : [45],
			45 : [94],
			46 : [93],
			47 : [95],
			93 : [40],
			94 : [40],
			95 : [40],
			},
	},
}

func _ready():
	pass

func is_animatronic_at(pos):
	for animatronic in get_children():
		if animatronic.get_name() == "sfx":
			continue
		if animatronic.position == pos:
			return true
	return false