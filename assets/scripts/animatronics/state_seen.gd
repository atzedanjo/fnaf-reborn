extends Node

var logger
var actor
var watch_timer
var wait_time

func _ready():
	logger = get_node("/root/office")
	actor = get_parent()

func enter():
	watch_timer = 0
	wait_time = 10
	logger.printlog("ID: " + actor.get_name() + " STATE_SEEN " + "WAIT: " + str(wait_time))

func update(delta):
	watch_timer += delta
	if watch_timer >= wait_time:
		actor.transition_to("UNSEEN")

func exit():
	pass