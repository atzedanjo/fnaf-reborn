extends Node

var actor
var wait_timer
var wait_time

func _ready():
	actor = get_parent()

func enter():
	wait_timer = 0
	wait_time = 100

func update(delta):
	wait_timer += delta
	if wait_timer < wait_time:
		return
	else:
		wait_timer = 0
		wait_time = 10
	if not get_node("../../").is_animatronic_at(10) and not get_node("../../").is_animatronic_at(20):
		actor.transition_to("UNSEEN")

func exit():
	pass