extends Node

var logger
var actor
var watch_timer
var wait_time


func _ready():
	logger = get_node("/root/office")
	actor = get_parent()

func enter():
	watch_timer = 0
	wait_time = rand_range(10, 30)
	logger.printlog("[color=" + actor.color + "]ID: " + actor.get_name() + " STATE_UNSEEN " + "WAIT: " + str(wait_time) + "[/color]")

func update(delta):
	watch_timer += delta
	if watch_timer >= wait_time:
		actor.transition_to("WALK")

func exit():
	pass