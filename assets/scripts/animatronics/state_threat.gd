extends Node

var actor
var threat_timer
var wait_time

var cam_open = false

func _ready():
	actor = get_parent()
	
func enter():
	threat_timer = 0
	wait_time = rand_range(3, 10)
	cam_open = false
	if not get_node("/root/office/cam").is_hidden():
		cam_open = true
		get_node("/root/office/cam").connect("visibility_changed", self, "_on_cam_visibility_changed", [], CONNECT_ONESHOT)
	else:
		get_node("../../sfx").play("stare")
	get_node("/root/office/office_scare/anim").play("scare")
	

func update(delta):
	if cam_open:
		return
	threat_timer += delta
	if threat_timer >= wait_time:
		get_node("/root/office/office_scare/anim").stop()
		get_node("../../sfx").stop_all()
		get_node("/root/office/black").hide()
		if get_node(actor.block_node).is_visible():
			actor.transition_to("WALK")
		else:
			get_node("/root/office/cam").hide()
			get_node("../../sfx").stop_all()
			get_node("/root/office")._on_jumpscare(actor.get_name())

func _on_cam_visibility_changed():
	cam_open = false
	get_node("../../sfx").play("stare")
#	get_node("/root/office/cam").disconnect("visibility_changed", self, "_on_cam_visibility_changed")

func exit():
	pass

