extends Node

var logger
var actor

func _ready():
	actor = get_parent()
	logger = get_node("/root/office")

func enter():
	var possible_destinations = actor.path_tree[actor.position]
	var next_pos = null
	var next_state = "UNSEEN"
	
	if possible_destinations.size() == 1:
		next_pos = possible_destinations[0]
	else:
		next_pos = possible_destinations[randi() % possible_destinations.size()]
	if next_pos in actor.threat_pos:
		next_state = "THREAT"
	actor.position = next_pos
	logger.printlog("ID: " + actor.get_name() + " WALK TO: " + str(actor.position))
	actor.transition_to(next_state)

func update(delta):
	pass

func exit():
	pass