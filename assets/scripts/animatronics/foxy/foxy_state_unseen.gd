extends Node

var logger
var actor
var watch_timer
var wait_time

var running_sfx
var in_floor = false

var cams

func _ready():
	logger = get_node("/root/office")
	actor = get_parent()
	cams = get_node("/root/office/cam")
	watch_timer = 0

func enter():
	if watch_timer == 0:
		wait_time = rand_range(10, 30)
	logger.printlog("id: " + actor.get_name() + " STATE_UNSEEN " + "WAIT: " + str(wait_time))

func update(delta):
	if cams.is_visible() and cams.cams[cams.current_cam].has_node(str(actor.position)):
		actor.transition_to("SEEN")
		return
	watch_timer += delta
	if watch_timer >= wait_time:
		in_floor = false
		watch_timer = 0
		actor.transition_to("WALK")

func exit():
	pass