extends Node

var actor
var threat_timer
var wait_time

func _ready():
	actor = get_parent()

func enter():
	threat_timer = 0
	wait_time = 3
	get_node("/root/office/office_scare/anim").play("scare")

func update(delta):
	threat_timer += delta
	if threat_timer >= wait_time:
		if get_node(actor.block_node).is_visible():
			get_node("/root/office/office_scare/anim").stop()
			get_node("/root/office/black").hide()
			actor.transition_to("WALK")
		else:
			get_node("/root/office")._on_jumpscare(actor.get_name())

func exit():
	pass