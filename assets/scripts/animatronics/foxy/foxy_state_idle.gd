extends Node

var actor
var wait_timer
var wait_time

func _ready():
	actor = get_parent()

func enter():
	wait_timer = 0
	wait_time = rand_range(30, 60)

func update(delta):
	wait_timer += delta
	if wait_timer >= wait_time:
		actor.transition_to("UNSEEN")

func exit():
	pass