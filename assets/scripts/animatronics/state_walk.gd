extends Node

var logger
var actor
var other_anim = {"0" : "2", "2" : "0"}

func _ready():
	actor = get_parent()
	logger = get_node("/root/office")

func enter():
	var possible_destinations = actor.path_tree[actor.position]
	var next_pos = null
	var next_state = "UNSEEN"
	
	if possible_destinations.size() == 1:
		next_pos = possible_destinations[0]
	else:
		next_pos = possible_destinations[randi() % possible_destinations.size()]
	if next_pos in actor.threat_pos:
		next_state = "THREAT"
		if other_anim.has(actor.get_name()):
			var tmp = get_node("../../" + other_anim[actor.get_name()])
			if tmp.position in tmp.threat_pos:
				actor.transition_to("UNSEEN")
				return
	actor.position = next_pos
	logger.printlog("ID: " + actor.get_name() + " WALK TO: " + str(actor.position))
	actor.transition_to(next_state)

func update(delta):
	pass

func exit():
	pass