extends Node2D

const BLANK = 0
const NIGHT = 1
const PAYCHECK = 2

var loader
var wait_frames
var time_max = 100 # msec
var current_scene = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)

func goto_scene(path, mode): # game requests to switch to this scene
	if mode == NIGHT:
		wait_frames = 180
		get_node("SamplePlayer").play("startday")
		get_node("stripes/anim").play("default")
		get_node("nights").set_frame(game_data.current_night)
		get_node("paycheck").hide()
		get_node("nights").show()
		
	elif mode == PAYCHECK:
		wait_frames = 3000
		get_node("SamplePlayer").play("paycheck")
		get_node("nights").hide()
		get_node("paycheck").show()
		get_node("paycheck/anim").play("fade_in")
	else:
		wait_frames = 180
		get_node("paycheck").hide()
		get_node("nights").hide()
	loader = ResourceLoader.load_interactive(path)
	if loader == null: # check for errors
		show_error()
		return
	set_process(true)
	current_scene.queue_free() # get rid of the old scene
	# start your "loading..." animation
	show()
#	wait_frames = 180

func _process(time):
	if loader == null:
		# no need to process anymore
		set_process(false)
		return
	# wait for frames to let the "loading" animation to show up
	if wait_frames > 0:
		wait_frames -= 1
		return
	var t = OS.get_ticks_msec()
	# use "time_max" to control how much time we block this thread
	while OS.get_ticks_msec() < t + time_max:
		# poll your loader
		var err = loader.poll()
		if err == ERR_FILE_EOF: # load finished
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break
		elif err == OK:
			update_progress()
		else: # error during loading
			show_error()
			loader = null
			break

func update_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()


func set_new_scene(scene_resource):
	hide()
	get_node("SamplePlayer").stop_all()
	current_scene = scene_resource.instance()
	get_node("/root").add_child(current_scene)