extends Control

onready var office = get_node("/root/office/office")
onready var power_meter = get_node("/root/office/hud/power/power_meter")
onready var power_left = get_node("/root/office/hud/power/power_left")

func _ready():
	set_process_input(true)

func _input(event):
	if office.get_frame() in [2,3]:
		return
	if event.is_action_pressed("flashlight"):
		office.set_frame(1)
	elif event.is_action_released("flashlight"):
		office.set_frame(0)

# left button
func _on_left_button_down():
	get_node("../light_sfx").play()
	power_meter.set_value(power_meter.get_value() + 20)
	power_left.usage_multiplier += 1
	office.set_frame(2)
	for node in get_node("../left_vent").get_children():
		node.set_modulate(Color(1,1,1))

func _on_left_button_up():
	get_node("../light_sfx").stop()
	power_meter.set_value(power_meter.get_value() - 20)
	power_left.usage_multiplier -= 1
	office.set_frame(0)
	for node in get_node("../left_vent").get_children():
		node.set_modulate(Color(0,0,0))

# rught button
func _on_right_button_down():
	get_node("../light_sfx").play()
	power_meter.set_value(power_meter.get_value() + 20)
	power_left.usage_multiplier += 1
	office.set_frame(3)
	for node in get_node("../left_vent").get_children():
		node.set_modulate(Color(1,1,1))

func _on_right_button_up():
	get_node("../light_sfx").stop()
	power_meter.set_value(power_meter.get_value() - 20)
	power_left.usage_multiplier -= 1
	office.set_frame(0)
	for node in get_node("../left_vent").get_children():
		node.set_modulate(Color(0,0,0))
