extends Camera2D

onready var left_scroll = get_node("left_scroll")
onready var right_scroll = get_node("right_scroll")

const MOVE_SPEED = 500
var move_dir = Vector2()

func _ready():
	left_scroll.connect("mouse_enter", self, "_on_left_scroll_mouse_enter")
	left_scroll.connect("mouse_exit", self, "_on_left_scroll_mouse_exit")
	right_scroll.connect("mouse_enter", self, "_on_right_scroll_mouse_enter")
	right_scroll.connect("mouse_exit", self, "_on_right_scroll_mouse_exit")
	set_fixed_process(true)


func _fixed_process(delta):
	var motion = move_dir * MOVE_SPEED
	translate(motion * delta)
	if get_pos().x <= 0:
		set_pos(Vector2(0, get_pos().y))
	if get_pos().x >= 320:
		set_pos(Vector2(320, get_pos().y))

func _on_left_scroll_mouse_enter():
	move_dir.x = -1

func _on_left_scroll_mouse_exit():
	move_dir.x = 0

func _on_right_scroll_mouse_enter():
	move_dir.x = 1
	
func _on_right_scroll_mouse_exit():
	move_dir.x = 0