extends Node2D

onready var anim_player = get_node("AnimationPlayer")
onready var new_game_btn = get_node("menu/new")
onready var continue_btn = get_node("menu/vbox/continue")

var freddy_timer = 0
var freddy_delay

func _ready():
	game_data.load_game()
#	get_node("menu/new_game/button").grab_focus()
	get_node("static").play("default")
	new_game_btn.connect("pressed", self, "_on_new_game_pressed")
	continue_btn.connect("pressed", self, "_on_continue_pressed")
	freddy_delay = 5
	set_process(true)

func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	freddy_timer += delta
	if freddy_timer > freddy_delay:
		freddy_timer = 0
		freddy_delay = rand_range(3, 20)
		anim_player.play("freddy2")
		anim_player.queue("freddy")
		if randf() < 0.5:
			get_node("glitch/anim").play("glitch")

func _on_new_game_pressed():
	game_data.current_night = 0
	var timer = Timer.new()
	add_child(timer)
	timer.set_wait_time(5)
	timer.start()
	timer.connect("timeout", self, "_change_scene")
	get_node("newspaper").show()

func _on_continue_pressed():
	_change_scene()

func _change_scene():
	scene_switcher.goto_scene("res://assets/scenes/office.tscn", scene_switcher.NIGHT)
