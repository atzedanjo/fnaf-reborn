extends TextureButton

onready var cam_nr = int(get_name())
onready var current_cam = get_node("../../..")

func _ready():
	connect("pressed", self, "_on_pressed")

func _on_pressed():
	get_node("../../../sfx").play("cam_btn")
	current_cam.switch_to(cam_nr)
