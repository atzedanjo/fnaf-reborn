extends Node2D

var logger

func _ready():
	randomize()
	logger = get_node("hud/debug_menu/HBoxContainer/log")
	logger.set_scroll_follow(true)
	printlog("Log started")
	get_node("hud/clock/hour").connect("player_won", self, "_on_player_won")
	get_node("hud/night/night_n").set_text(str(game_data.current_night+1))
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		scene_switcher.goto_scene("res://assets/scenes/title_screen.tscn", scene_switcher.BLANK)

func stop_sounds():
	get_node("fan_player").stop()
	get_node("ambient2").stop()
	get_node("coldpress").stop()

func _on_player_won():
	stop_sounds()
	get_node("hud/end_shift").show()
	get_tree().set_pause(true)

func _on_jumpscare(who):
	stop_sounds()
	get_node("office_scare").hide()
	get_node("hud/jumpscares/anim").play(who)
	get_tree().set_pause(true)

func printlog(msg):
	if logger:
		logger.append_bbcode(str(msg,"\n"))
#		logger.add_text("###############")
#		logger.newline()
#		logger.add_text(msg)
#		logger.newline()